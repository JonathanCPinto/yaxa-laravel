<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class BookController extends Controller
{
  /**
   * Index books
   */
  public function index(){
      $books = Book::all();
      return view('books.index', compact('books'));
  }
}
