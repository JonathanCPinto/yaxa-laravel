<?php

namespace App\Http\Controllers;
use Validator;
use App\Role;

use Illuminate\Http\Request;

class RoleController extends Controller
{
  /**
   * Index roles
   */
  public function index(){
      $roles = Role::all();
      return view('roles.index', compact('roles'));
  }

  /**
   * View create role
   */
  public function create(){
      return view('roles.create');
  }

  /**
   * Store new role
   */
  public function store(Request $request){
      $validator = Validator::make($request->all(), Role::$rules);
      if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
      }
      $user = new Role();
      $user->name = $request->get('name');
      $user->description = $request->get('description');
      $user->save();
      return redirect()->back()->with('message', 'Role saved succesfully !');
  }
}
