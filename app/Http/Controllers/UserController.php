<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;

class UserController extends Controller
{

    /**
     * Index users
     */
    public function index(){
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * View create user
     */
    public function create(){
        return view('users.create');
    }

    /**
     * Store new user
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), User::$rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->email_verified_at = now();
        $user->remember_token = \Str::random(10);
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return redirect()->back()->with('message', 'User saved succesfully !');
    }
}
