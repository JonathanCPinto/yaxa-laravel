<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class SuperUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(\Auth::check())
        {
          $user = \Auth::user();
          $role_superuser = Role::SuperUser()->first()->id;
          if(in_array($role_superuser, $user->roles()->pluck('role_id')->toArray())){
              return $next($request);
          }
        }
        return redirect()->back();
    }
}
