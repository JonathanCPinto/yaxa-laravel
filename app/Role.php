<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public static $rules = [
            'name' => 'min:3|max:255|required',
            'description' => 'required'
    ];

    const SUPER_USER = 'super_user';
    const USER = 'user';

    public function scopeSuperUser($query)
    {
        return $query->where('name', '=', self::SUPER_USER);
    }

    public function scopeUser($query)
    {
        return $query->where('name', '=', self::USER);
    }
}
