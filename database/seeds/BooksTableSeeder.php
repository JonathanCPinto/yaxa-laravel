<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $book = new Book();
      $book->isbn = '111222';
      $book->title = "Cien años de Soledad";
      $book->author = "Gabriel García Márquez";
      $book->pages = "1550";
      $book->save();

      $book = new Book();
      $book->isbn = '222111';
      $book->title = "Rayuela";
      $book->author = "Julio Cortázar";
      $book->pages = "300";
      $book->save();
    }
}
