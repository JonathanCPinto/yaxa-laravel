<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Super Usuario
      $user = new Role();
      $user->name = 'super_user';
      $user->description = "Super User";
      $user->save();
      // Usuario
      $user = new Role();
      $user->name = 'user';
      $user->description = "User";
      $user->save();

    }
}
