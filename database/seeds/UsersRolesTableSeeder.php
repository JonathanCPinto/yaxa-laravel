<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\UserRole;

class UsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_superuser = Role::where('name', 'super_user')->first();
      $role_user = Role::where('name', 'user')->first();
      $super_user = User::where('email', 'superusuario@example.com')->first();
      $user = User::where('email', 'usuario@example.com')->first();

      $new_role = new UserRole();
      $new_role->user_id = $super_user->id;
      $new_role->role_id = $role_superuser->id;
      $new_role->save();

      $new_role = new UserRole();
      $new_role->user_id = $user->id;
      $new_role->role_id = $role_user->id;
      $new_role->save();

    }
}
