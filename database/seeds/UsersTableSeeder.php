<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Super Usuario
        $user = new User();
        $user->name = 'Super Usuario';
        $user->email = "superusuario@example.com";
        $user->email_verified_at = now();
        $user->remember_token = Str::random(10);
        $user->password = bcrypt("123");
        $user->save();
        // Usuario
        $user = new User();
        $user->name = 'Usuario';
        $user->email = "usuario@example.com";
        $user->email_verified_at = now();
        $user->remember_token = Str::random(10);
        $user->password = bcrypt("123");
        $user->save();
    }
}
