@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Index Books</div>
                <div class="card-body">
                  <table class="table table-th-block bg-color-white">
                  	<thead class="border-thead">
                  		<tr class="tr-table">
                  			<th></th>
                  			<th>ISBN</th>
                  			<th>Title</th>
                  			<th>Author</th>
                  			<th>Total Pages</th>
                  		</tr>
                  	</thead>
                  	<tbody class="text-left">
                      @foreach ($books as $book)
                          <tr>
                              <td>{{ $book->id }}</td>
                              <td>{{ $book->isbn }}</td>
                              <td>{{ $book->title }}</td>
                              <td>{{ $book->author }}</td>
                              <td>{{ $book->pages }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
