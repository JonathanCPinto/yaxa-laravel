@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register Role</div>

                <div class="card-body">
                    @include('common.success')
                    @include('common.errors')

                    <form method="post" action="{{ route('roles.store') }}">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputName1">Name</label>
                        <input type="text" class="form-control" name="name"  id="exampleNameName1" aria-describedby="nameHelp" placeholder="Enter name">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text" class="form-control" name="description" id="exampleInputEmail1" aria-describedby="descriptionHelp" placeholder="Enter description">

                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
