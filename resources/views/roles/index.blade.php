@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Index Roles</div>
                <div class="card-body">
                  <table class="table table-th-block bg-color-white">
                  	<thead class="border-thead">
                  		<tr class="tr-table">
                  			<th>Id</th>
                  			<th>Description</th>
                  		</tr>
                  	</thead>
                  	<tbody class="text-left">
                      @foreach ($roles as $role)
                          <tr>
                              <td>{{ $role->id }}</td>
                              <td>{{ $role->description }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  <a href="{{ route('roles.create') }}" class="btn btn-success">Register New Role</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
