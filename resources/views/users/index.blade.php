@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Index Users</div>
                <div class="card-body">
                  <table class="table table-th-block bg-color-white">
                  	<thead class="border-thead">
                  		<tr class="tr-table">
                  			<th>Name</th>
                  			<th>Email</th>
                  			<th>Actions</th>
                  		</tr>
                  	</thead>
                  	<tbody class="text-left">
                      @foreach ($users as $user)
                          <tr>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>
                                  <a class="btn btn-info btn-sm btn-perspective " href="#!" onclick="alert('Nothing is here XD');"><i class="fa fa-pencil"></i>
                                     Edit
                                  </a>
                              </td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  <a href="{{ route('users.create') }}" class="btn btn-success">Register New User</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
