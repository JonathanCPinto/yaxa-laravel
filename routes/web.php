<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['super_user']], function () {
              // USERS
              Route::get('/users', 'UserController@index')->name('users.index');
              Route::get('/users/create', 'UserController@create')->name('users.create');
              Route::post('/users/store', 'UserController@store')->name('users.store');

              // ROLES
              Route::get('/roles', 'RoleController@index')->name('roles.index');
              Route::get('/roles/create', 'RoleController@create')->name('roles.create');
              Route::post('/roles/store', 'RoleController@store')->name('roles.store');

              // BOOKS
              Route::get('/books', 'BookController@index')->name('books.index');

});

Route::group(['middleware' => ['user']], function () {
              // BOOKS
              Route::get('/books', 'BookController@index')->name('books.index');

});

Route::get('/home', 'HomeController@index')->name('home');
